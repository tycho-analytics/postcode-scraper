# BUILD.R ####
#
# Copyright (C) 2019 Tycho Analytics
# Written by Pete Abriani Jensen <pete@tychoanalytics.com>
# Created 30 Aug 2019. Last updated 18 Sep 2019.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# DESCRIPTION
#
# This script builds a catalogue of postcodes from scraped webpages
#
# REQUIRES
#
#   - dplyr
#   - stringr
#
# IMPORTS
#
#   - code/functions.R
#   - data/html/[date]/[0000-9999].html
#
# EXPORTS
#
#   - data/html/postcodes-[date].csv
#
# CHANGE LOG
#
#   - 2019-09-17: Added GNU GPLv3 copyright notice
#

# 1. BEGIN SCRIPT ####

# Verbosity
{
    message("")
    message(paste("BEGIN SCRIPT: build.R |", Sys.time()))
    message("")
}

# Clean up global environment
{
    message("> Cleaning up global environment")
    rm(list = setdiff(ls(), "verbose")); gc()
}

# Load libraries
{
    message("> Loading libraries")
    library(stringr)
    source("code/functions.R")
}

# Set parameters
{
    date <- NULL
}

# 2. BUILD CATALOGUE ####

# Build postcodes catalogue
{
    message("> Building postcodes catalogue")
    postcodes <- build_catalogue(date)
    message("")
}

# Save catalogue to file
{
    file <- paste0("data/postcodes-", date, ".csv")
    message(paste("> Saving catalogue to", file))
    write.csv(postcodes, file, na = "", quote = F, row.names = F)
}

# 3. END SCRIPT ####

# Verbosity
{
    message("")
    message(paste("END SCRIPT: build.R |", Sys.time()))
    message("")
}
